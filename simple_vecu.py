from threading import Thread
import py_cui
import time
import can

class SimpleCUI:
    def __init__(self, master: py_cui.PyCUI):
        self.master = master
        self.vecu_zero = self.master.add_scroll_menu('VECU-0 (recv)', 0, 0, row_span=6, column_span=4)

    def add_to_top(self, ecu, text):
        self.vecu_zero.get_item_list().insert(0,text)
        # todo : delete old items

def vecu_zero_recv(s):
    can_interface = 'vcan0'
    bus = can.interface.Bus(can_interface, bustype='socketcan')
    while 1:
        message = bus.recv()
        id = message.arbitration_id
        data = ''.join('{:02x}'.format(x) for x in message.data)
        s.add_to_top(0, str(message)) # "%d#%s" % (id, data))

root = py_cui.PyCUI(7, 6)
root.set_title('CAN SIMULATION')
root.set_refresh_timeout(0.5)

s = SimpleCUI(root)
Thread(target=vecu_zero_recv, args=(s,)).start()
root.start()