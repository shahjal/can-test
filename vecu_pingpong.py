from random import randrange
from threading import Thread
import py_cui
import time
import can

PAUSED = False
EXITING = False

class SimpleCUI:
    def __init__(self, master: py_cui.PyCUI):
        self.master = master
        self.vecu_zero = self.master.add_scroll_menu('VECU-0', 0, 0, row_span=6, column_span=2)
        self.vecu_one = self.master.add_scroll_menu('VECU-1', 0, 3, row_span=6, column_span=2)

    def add_to_top(self, ecu, text):
        if ecu == 0 :
            self.vecu_zero.get_item_list().insert(0,text)
        else:
            self.vecu_one.get_item_list().insert(0,text)
        # todo : delete old items

def send_msg(bus, id, data):
    message = can.Message(arbitration_id=id, data=data, is_extended_id=False)
    bus.send(message)

def send_ping(bus):
    send_msg(bus, 1337, bytearray([0x50,0x49,0x4e,0x47]))

def send_pong(bus):
    send_msg(bus, 1337, bytearray([0x50,0x4f,0x4e,0x47]))

def vecu_zero(s, start):
    can_interface = 'vcan0'
    bus = can.interface.Bus(can_interface, bustype='socketcan')

    if start:
        # initiate the game
        time.sleep(1)
        send_ping(bus)

    while 1:
        if EXITING:
            # wake up the other vecu from recv sleep before exit
            bus.send(can.Message())
            break

        message = bus.recv()
        id = message.arbitration_id
        data = ''.join(map(chr, message.data))

        if id == 1337 and data == 'PONG':
            s.add_to_top(0, "%d#%s\t|\tSENDING PING" % (id, data))
            send_ping(bus)
        elif id == 1337 and data == 'PING':
            s.add_to_top(0, "%d#%s\t|\tSENDING PONG" % (id, data))
            send_pong(bus)
        else:
            data = ''.join('{:02x}'.format(x) for x in message.data)
            s.add_to_top(0, "%d#%s" % (id, data))
        
def vecu_one(s, start):
    can_interface = 'vcan0'
    bus = can.interface.Bus(can_interface, bustype='socketcan')

    if start:
        # initiate the game
        time.sleep(1)
        send_ping(bus)

    while 1:
        if EXITING:
            # wake up the other vecu from recv sleep before exit
            bus.send(can.Message())
            break

        message = bus.recv()
        id = message.arbitration_id
        data = ''.join(map(chr, message.data))

        if id == 1337 and data == 'PONG':
            s.add_to_top(1, "%d#%s\t|\tSENDING PING" % (id, data))
            send_ping(bus)
        elif id == 1337 and data == 'PING':
            s.add_to_top(1, "%d#%s\t|\tSENDING PONG" % (id, data))
            send_pong(bus)
        else:
            data = ''.join('{:02x}'.format(x) for x in message.data)
            s.add_to_top(1, "%d#%s" % (id, data))

        time.sleep(1)


def pause():
    global PAUSED
    PAUSED = not PAUSED

def exiting():
    global EXITING
    EXITING = not EXITING

root = py_cui.PyCUI(7, 6)
root.set_title('CAN SIMULATION')
root.run_on_exit(exiting)
root.add_key_command(py_cui.keys.KEY_SPACE, pause)
root.set_refresh_timeout(0.5)

s = SimpleCUI(root)
Thread(target=vecu_zero, args=(s,1)).start()
Thread(target=vecu_one, args=(s,0)).start()
root.start()