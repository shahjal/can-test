from random import randrange
from threading import Thread
import py_cui
import time
import can

PAUSED = False
EXITING = False

class SimpleCUI:
    def __init__(self, master: py_cui.PyCUI):
        self.master = master
        self.vecu_zero = self.master.add_scroll_menu('VECU-0 (recv)', 0, 0, row_span=6, column_span=4)
        self.vecu_one = self.master.add_scroll_menu('VECU-1 (send)', 0, 4, row_span=6, column_span=1)

    def add_to_top(self, ecu, text):
        if ecu == 0 :
            self.vecu_zero.get_item_list().insert(0,text)
        else:
            self.vecu_one.get_item_list().insert(0,text)
        # todo : delete old items

def vecu_zero_recv(s):
    can_interface = 'vcan0'
    bus = can.interface.Bus(can_interface, bustype='socketcan')
    while 1:
        if EXITING:
            break
        message = bus.recv()
        id = message.arbitration_id
        data = ''.join('{:02x}'.format(x) for x in message.data)
        s.add_to_top(0, str(message)) # "%d#%s" % (id, data))

def vecu_one_send(s):
    can_interface = 'vcan0'
    bus = can.interface.Bus(can_interface, bustype='socketcan')
    while 1:
        time.sleep(1)
        if EXITING:
            # wake up vecu_zero from recv sleep before exit
            bus.send(can.Message())
            break
        if PAUSED:
            continue

        id = randrange(255)
        data = [randrange(255) for i in range(0,randrange(1,8))]
        message = can.Message(arbitration_id=id, data=data, is_extended_id=False)
        bus.send(message)

        data = ''.join('{:02x}'.format(x) for x in data)
        s.add_to_top(1, "%s#%s" % ('{:04x}'.format(id), data))

def pause():
    global PAUSED
    PAUSED = not PAUSED

def exiting():
    global EXITING
    EXITING = not EXITING

root = py_cui.PyCUI(7, 6)
root.set_title('CAN SIMULATION')
root.run_on_exit(exiting)
root.add_key_command(py_cui.keys.KEY_SPACE, pause)
root.set_refresh_timeout(0.5)

s = SimpleCUI(root)
Thread(target=vecu_zero_recv, args=(s,)).start()
Thread(target=vecu_one_send, args=(s,)).start()
root.start()